Dieses Projekt bietet dir die Möglichkeit, Spieler-Statistiken abzuspeichern und diese
mit dem /Stats Command wieder abzurufen.

Vielen Dank an:

- Eisphoenix | Lukas (Optimierung des Codes)
- Deppeloper | Sebastian (Optimierung des Codes & GitLab Einführung)
