package de.spigotplugins.statsmanager.manager;

import java.util.UUID;

/**
 * Created by Pascal Falk on 03.12.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public final class PlayerStatistic {
	private final UUID uniqueID;
	private int killCounter;
	private int deathCounter;

	public PlayerStatistic(final UUID uniqueID) {
		this(uniqueID, 0, 0);
	}

	public PlayerStatistic(final UUID uniqueID, final int killCounter, final int deathCounter) {
		this.uniqueID = uniqueID;
		this.killCounter = killCounter;
		this.deathCounter = deathCounter;
	}

	public int incrementDeathCount() {
		return ++deathCounter;
	}

	public int incrementKillCount() {
		return ++killCounter;
	}

	public UUID getUniqueID() {
		return uniqueID;
	}

	public int getDeathCount() {
		return deathCounter;
	}

	public int getKillCount() {
		return killCounter;
	}
}
