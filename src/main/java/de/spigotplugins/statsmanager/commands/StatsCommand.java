package de.spigotplugins.statsmanager.commands;

import de.spigotplugins.statsmanager.StatsManager;
import de.spigotplugins.statsmanager.manager.PlayerStatistic;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static de.spigotplugins.statsmanager.StatsManager.getPrefix;

/**
 * Created by Pascal Falk on 03.12.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public final class StatsCommand implements CommandExecutor {
	private final StatsManager statsManager;

	public StatsCommand(final StatsManager statsManager) {
		this.statsManager = statsManager;
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command command, final String label, final String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("Du musst ein Spieler sein, um diesen Command ausführen zu dürfen.");
			return true;
		}

		if (args.length != 0) {
			sender.sendMessage(getPrefix() + "§7Falsche Benutzung. Tippe §e/Stats");
			return true;
		}

		final PlayerStatistic playerStatistic = statsManager.getStatistic(((Player) sender).getUniqueId());
		sender.sendMessage(getPrefix() + "§eKills: " + playerStatistic.getKillCount());
		sender.sendMessage(getPrefix() + "§eDeaths: " + playerStatistic.getDeathCount());

		return true;
	}
}
